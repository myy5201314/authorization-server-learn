package com.xueliman.iov.auth_server_resource_common.other;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author zxg
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SingleResultBundle<T>  {
    private T data;
    private int code = 200;
    protected boolean success;
    protected String message;


    public void setSuccess(final boolean success) {
        this.success = success;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public String getMessage() {
        return this.message;
    }

    public SingleResultBundle() {
    }

    public SingleResultBundle(T result) {
        this.success = true;
        this.data = result;
        this.code = ResultCode.SUCCESS.code();
        this.message = ResultCode.SUCCESS.getMessage();
    }

    public SingleResultBundle(boolean success, String errorMsg) {
        this.success = success;
        this.code = success ? ResultCode.SUCCESS.code() : ResultCode.FAILURE.code();
        this.message = success ? ResultCode.SUCCESS.getMessage() : errorMsg;
    }

    public SingleResultBundle(ResultCode resultCode) {
        this.success = resultCode.success();
        this.code = resultCode.code();
        this.message = resultCode.getMessage();
    }

    public SingleResultBundle(int code, String message, T data, boolean success) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.success = success;
    }

    public static <T> SingleResultBundle<T> success(T data) {
        return new SingleResultBundle(ResultCode.SUCCESS.code(), ResultCode.SUCCESS.getMessage(), data, true);
    }

    public static <T> SingleResultBundle<T> success(T data, String message) {
        return new SingleResultBundle(ResultCode.SUCCESS.code(), message, data, true);
    }

    public static <T> SingleResultBundle<T> failed(ResultCode resultCode) {
        return new SingleResultBundle(resultCode.code(), resultCode.getMessage(), (Object)null, false);
    }

    public static <T> SingleResultBundle<T> failed(ResultCode resultCode, String errMsg) {
        return new SingleResultBundle(resultCode.code(), errMsg, (Object)null, false);
    }

    public static <T> SingleResultBundle<T> failed(String errMsg) {
        return new SingleResultBundle(ResultCode.FAILURE.code(), errMsg, (Object)null, false);
    }

    public static <T> SingleResultBundle<T> failed(int code, String errMsg) {
        return new SingleResultBundle(code, errMsg, (Object)null, false);
    }

    public static <T> SingleResultBundle<T> failed() {
        return failed(ResultCode.FAILURE);
    }

    public void setData(final T data) {
        this.data = data;
    }

    public void setCode(final int code) {
        this.code = code;
    }

    public T getData() {
        return this.data;
    }

    public int getCode() {
        return this.code;
    }
}
