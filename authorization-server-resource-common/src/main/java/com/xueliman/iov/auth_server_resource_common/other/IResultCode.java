package com.xueliman.iov.auth_server_resource_common.other;

/**
 * @author zxg
 */
public interface IResultCode {
    boolean success();

    int code();

    String getMessage();
}
